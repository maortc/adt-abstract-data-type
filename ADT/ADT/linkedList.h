#ifndef LINKEDLIST_H
#define LINKEDLIST_H
#include <iostream>
using namespace std;


/* a queue contains positive integer values. */
typedef struct Node {
	int data;
	Node* next;
} Node;

void push(Node** head, int node_data);
void insertAfter(Node* prev_node, int node_data);
void append(Node** head, int node_data);

void displayList(Node* node);



#endif /* LINKEDLIST_H */