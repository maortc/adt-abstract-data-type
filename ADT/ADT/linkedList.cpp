#include "linkedList.h"


// Insert a new node in front of the list
void push(Node** head, int node_data)
{

	Node* newNode = new Node; // Create and allocate node

	newNode->data = node_data;
	newNode->next = (*head);

	(*head) = newNode; // Move the head to point to the new node
}


// Insert new node after a given node
void insertAfter(Node* prev_node, int node_data)
{
	
	if (prev_node == NULL) // Check if the given prev_node is NULL
	{
		cout << "the given previous node is required, cannot be NULL";
		return;
	}

	Node* newNode = new Node;
	newNode->data = node_data;
	newNode->next = prev_node->next;
	prev_node->next = newNode;
}

// Insert new node at the end of the linked list
void append(Node** head, int node_data)
{
	Node* newNode = new Node;
	Node* last = *head; 

	newNode->data = node_data;
	newNode->next = NULL;

	if (*head == NULL) // If list is empty, new node becomes first node
	{
		*head = newNode;
		return;
	}

	while (last->next != NULL) //  traverse till the last node
		last = last->next;

	last->next = newNode; // Change the next of last nod
	return;
}

// Display linked list contents
void displayList(Node* node)
{
	//traverse the list to display each node
	while (node != NULL)
	{
		cout << node->data << "-->";
		node = node->next;
	}
}