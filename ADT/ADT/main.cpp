#include "queue.h"
#include "linkedList.h"
#include <iostream>
#include <cstdlib>

using std::cout;
using std::endl;

void checkQueue();
void checkLinkedList();
int main()
{
	string choice = "";
	while (true)
	{
		cout << "1 - Check queue ADT" << endl;
		cout << "2 - check linkedList ADT" << endl;
		cin >> choice;

		if (choice == "1")
			checkQueue();
		else if (choice == "2")
			checkLinkedList();
		else
			cout << "Wrong choice try again!" << endl;
		cout << endl;
	}
	
	return 0;
}

void checkQueue() {
	Queue* q = new Queue;
	initQueue(q, 3); //initiate queue of size 3
	enqueue(q, 1);
	enqueue(q, 4);
	enqueue(q, 9);

	while (!isEmpty(q))
	{
		cout << dequeue(q) << endl;
	}

	cleanQueue(q);
	delete q;

}

void checkLinkedList()
{
	Node* head = NULL;

	append(&head, 10);
	push(&head, 20);
	push(&head, 30);
	append(&head, 40); 
	insertAfter(head->next, 50);

	cout << "linked list : " << endl;
	displayList(head);

}